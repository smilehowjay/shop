package com.how.shopping.api.service.Impl;

import com.how.shopping.api.pojo.entity.Person;
import com.how.shopping.api.repository.PersonRepository;
import com.how.shopping.api.repository.mapper.PersonMapper;
import com.how.shopping.api.service.PersonService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService{

    @Resource
    private PersonMapper personMapper;

    @Resource
    private PersonRepository personRepository;

    @Override
    public Person selectPersonById(Integer id){
        return personMapper.selectPersonById(id);
    }

    @Override
    public List<Person> selectAll(){
        return personMapper.selectAll();
    }

    @Override
    public void insert(Person person){
        personMapper.insert(person);
    }

    @Override
    public Long update(Person person){
        return personMapper.update(person);
    }

    @Override
    public Long delete(Long id){
       return personMapper.delete(id);
    }

    @Override
    public Optional<Person> findById(Long id) {
        return personRepository.findById(id);
    }
}
