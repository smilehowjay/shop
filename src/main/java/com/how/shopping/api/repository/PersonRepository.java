package com.how.shopping.api.repository;

import com.how.shopping.api.pojo.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person,Long>, JpaSpecificationExecutor<Person> {
    Optional<Person> findById(Long id);
}
