package com.how.shopping.api.pojo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Person implements Serializable {
    private static final long serialVersionUID = -7572697046940288333L;

    @Id
    private Long id;

    private String name;

    private Integer age;

}
