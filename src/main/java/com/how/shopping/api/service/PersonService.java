package com.how.shopping.api.service;

import com.how.shopping.api.pojo.entity.Person;

import java.util.List;
import java.util.Optional;

public interface PersonService {

    Person selectPersonById(Integer id);

    List<Person> selectAll();

    void insert(Person person);

    Long update(Person person);

    Long delete(Long id);

    Optional<Person> findById(Long id);
}
