package com.how.shopping.api.repository.mapper;

import com.how.shopping.api.pojo.entity.Person;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface PersonMapper {

    Person selectPersonById(Integer id);

    List<Person> selectAll();

    void insert(Person person);

    Long update(Person person);

    Long delete(Long id);
}
