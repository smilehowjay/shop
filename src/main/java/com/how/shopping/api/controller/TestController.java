package com.how.shopping.api.controller;

import com.how.shopping.api.pojo.entity.Person;
import com.how.shopping.api.service.PersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
@EnableAutoConfiguration
@Api("Test")
public class TestController {

    @Resource
    private PersonService personService;

    @GetMapping("/MapperTest")
    @ApiOperation(value ="測試" , notes = "測試１２４")
    public List<Person> MapperTest(){

        return personService.selectAll();
    }

    @GetMapping("/JpaTest")
    @ApiOperation(value ="測試" , notes = "測試１２４")
    public Person JpaTest(){
        System.out.println("test");
        return personService.findById(2L).get();
    }

}
